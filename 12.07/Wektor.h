#ifndef WEKTOR_H
#define WEKTOR_H
#include <iostream>
using namespace std;

class Wektor
{
private:
    int n;
    int *tab[];
public:
    Wektor();
    Wektor(int a);
    Wektor(int a,int b);
    Wektor(const Wektor&);
    Wektor& operator=(const Wektor&);
    ~Wektor();
};



#endif // WEKTOR_H
