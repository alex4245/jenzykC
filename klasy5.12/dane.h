#ifndef DANE_H
#define DANE_H
#include <iostream>
using namespace std;

class Dane
    {
    private:
        string m_imie;
        string m_nazwisko;
        string m_telefon;
        string m_adress;
        string m_data;
    public:
        Dane(string imie,string nazwisko,string telefon,string adress,string data);
        ~Dane();
        void SetParamet(string imie,string nazwisko,string telefon,string adress,string data);
        string SetImie();
        string SetNazwisko();
        string SetTelefon();
        string SetAdress();
        string SetData();
        void zapis();
        void wyswiet();

    };

#endif // DANE_H
