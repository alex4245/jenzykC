/***************************************************************
 * Name:      klassGuiMain.h
 * Purpose:   Defines Application Frame
 * Author:     ()
 * Created:   2016-11-28
 * Copyright:  ()
 * License:
 **************************************************************/

#ifndef KLASSGUIMAIN_H
#define KLASSGUIMAIN_H

//(*Headers(klassGuiFrame)
#include <wx/button.h>
#include <wx/menu.h>
#include <wx/statusbr.h>
#include <wx/frame.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
//*)

class klassGuiFrame: public wxFrame
{
    public:

        klassGuiFrame(wxWindow* parent,wxWindowID id = -1);
        virtual ~klassGuiFrame();

    private:

        //(*Handlers(klassGuiFrame)
        void OnQuit(wxCommandEvent& event);
        void OnAbout(wxCommandEvent& event);
        void OnButton1Click(wxCommandEvent& event);
        void OnTextCtrl3Text(wxCommandEvent& event);
        //*)

        //(*Identifiers(klassGuiFrame)
        static const long ID_TEXTCTRL1;
        static const long ID_STATICTEXT1;
        static const long ID_TEXTCTRL2;
        static const long ID_TEXTCTRL3;
        static const long ID_TEXTCTRL4;
        static const long ID_TEXTCTRL5;
        static const long ID_TEXTCTRL6;
        static const long ID_BUTTON1;
        static const long ID_STATICTEXT2;
        static const long ID_STATICTEXT3;
        static const long ID_STATICTEXT4;
        static const long ID_STATICTEXT5;
        static const long ID_TEXTCTRL7;
        static const long idMenuQuit;
        static const long idMenuAbout;
        static const long ID_STATUSBAR1;
        //*)

        //(*Declarations(klassGuiFrame)
        wxTextCtrl* TextCtrl3;
        wxStatusBar* StatusBar1;
        wxButton* Button1;
        wxStaticText* StaticText1;
        wxTextCtrl* TextCtrl5;
        wxStaticText* StaticText3;
        wxTextCtrl* TextCtrl6;
        wxTextCtrl* TextCtrl1;
        wxStaticText* StaticText4;
        wxStaticText* StaticText5;
        wxStaticText* StaticText2;
        wxTextCtrl* TextCtrl7;
        wxTextCtrl* TextCtrl4;
        wxTextCtrl* TextCtrl2;
        //*)

        DECLARE_EVENT_TABLE()
};

#endif // KLASSGUIMAIN_H
