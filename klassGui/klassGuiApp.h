/***************************************************************
 * Name:      klassGuiApp.h
 * Purpose:   Defines Application Class
 * Author:     ()
 * Created:   2016-11-28
 * Copyright:  ()
 * License:
 **************************************************************/

#ifndef KLASSGUIAPP_H
#define KLASSGUIAPP_H

#include <wx/app.h>

class klassGuiApp : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // KLASSGUIAPP_H
