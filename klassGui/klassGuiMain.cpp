/***************************************************************
 * Name:      klassGuiMain.cpp
 * Purpose:   Code for Application Frame
 * Author:     ()
 * Created:   2016-11-28
 * Copyright:  ()
 * License:
 **************************************************************/

#include "klassGuiMain.h"
#include <wx/msgdlg.h>
#include <fstream>
using namespace std;
//(*InternalHeaders(klassGuiFrame)
#include <wx/string.h>
#include <wx/intl.h>
//*)

//helper functions
enum wxbuildinfoformat {
    short_f, long_f };

wxString wxbuildinfo(wxbuildinfoformat format)
{
    wxString wxbuild(wxVERSION_STRING);

    if (format == long_f )
    {
#if defined(__WXMSW__)
        wxbuild << _T("-Windows");
#elif defined(__UNIX__)
        wxbuild << _T("-Linux");
#endif

#if wxUSE_UNICODE
        wxbuild << _T("-Unicode build");
#else
        wxbuild << _T("-ANSI build");
#endif // wxUSE_UNICODE
    }

    return wxbuild;
}

//(*IdInit(klassGuiFrame)
const long klassGuiFrame::ID_TEXTCTRL1 = wxNewId();
const long klassGuiFrame::ID_STATICTEXT1 = wxNewId();
const long klassGuiFrame::ID_TEXTCTRL2 = wxNewId();
const long klassGuiFrame::ID_TEXTCTRL3 = wxNewId();
const long klassGuiFrame::ID_TEXTCTRL4 = wxNewId();
const long klassGuiFrame::ID_TEXTCTRL5 = wxNewId();
const long klassGuiFrame::ID_TEXTCTRL6 = wxNewId();
const long klassGuiFrame::ID_BUTTON1 = wxNewId();
const long klassGuiFrame::ID_STATICTEXT2 = wxNewId();
const long klassGuiFrame::ID_STATICTEXT3 = wxNewId();
const long klassGuiFrame::ID_STATICTEXT4 = wxNewId();
const long klassGuiFrame::ID_STATICTEXT5 = wxNewId();
const long klassGuiFrame::ID_TEXTCTRL7 = wxNewId();
const long klassGuiFrame::idMenuQuit = wxNewId();
const long klassGuiFrame::idMenuAbout = wxNewId();
const long klassGuiFrame::ID_STATUSBAR1 = wxNewId();
//*)

BEGIN_EVENT_TABLE(klassGuiFrame,wxFrame)
    //(*EventTable(klassGuiFrame)
    //*)
END_EVENT_TABLE()
class Dane
    {
    public:
        wxString imie;
        wxString nazwisko;
        wxString telefon;
        wxString adress;
        wxString data;
        /*void zapis()
        {
            imie=TextCtrl1->GetValue();
            nazwisko=TextCtrl2->GetValue();
            telefon=TextCtrl3->GetValue();
            adress=TextCtrl4->GetValue();
            data=TextCtrl5->GetValue();
            /*cout<<"Enter name: "<<endl;
            cin>>imie;
            cout<<"Enter surname: "<<endl;
            cin>>nazwisko;
            cout<<"Enter telefon: "<<endl;
            cin>>telefon;
            cout<<"Enter adress: "<<endl;
            cin>>adress;
            cout<<"Enter data: "<<endl;
            cin>>data;
        };
        void wyswiet()
        {
            cout<<imie<<endl;
            cout<<nazwisko<<endl;
            cout<<telefon<<endl;
            cout<<adress<<endl;
            cout<<data<<endl;
        };*/
    };

klassGuiFrame::klassGuiFrame(wxWindow* parent,wxWindowID id)
{
    //(*Initialize(klassGuiFrame)
    wxMenuItem* MenuItem2;
    wxMenuItem* MenuItem1;
    wxMenu* Menu1;
    wxMenuBar* MenuBar1;
    wxMenu* Menu2;

    Create(parent, id, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxDEFAULT_FRAME_STYLE, _T("id"));
    TextCtrl1 = new wxTextCtrl(this, ID_TEXTCTRL1, wxEmptyString, wxPoint(168,128), wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL1"));
    StaticText1 = new wxStaticText(this, ID_STATICTEXT1, _("Imie"), wxPoint(96,136), wxDefaultSize, 0, _T("ID_STATICTEXT1"));
    TextCtrl2 = new wxTextCtrl(this, ID_TEXTCTRL2, wxEmptyString, wxPoint(168,168), wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL2"));
    TextCtrl3 = new wxTextCtrl(this, ID_TEXTCTRL3, wxEmptyString, wxPoint(168,208), wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL3"));
    TextCtrl4 = new wxTextCtrl(this, ID_TEXTCTRL4, wxEmptyString, wxPoint(168,248), wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL4"));
    TextCtrl5 = new wxTextCtrl(this, ID_TEXTCTRL5, wxEmptyString, wxPoint(168,288), wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL5"));
    TextCtrl6 = new wxTextCtrl(this, ID_TEXTCTRL6, wxEmptyString, wxPoint(88,336), wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL6"));
    Button1 = new wxButton(this, ID_BUTTON1, _("Label"), wxPoint(160,392), wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON1"));
    StaticText2 = new wxStaticText(this, ID_STATICTEXT2, _("Nazwisko"), wxPoint(96,176), wxDefaultSize, 0, _T("ID_STATICTEXT2"));
    StaticText3 = new wxStaticText(this, ID_STATICTEXT3, _("Telefon"), wxPoint(96,216), wxDefaultSize, 0, _T("ID_STATICTEXT3"));
    StaticText4 = new wxStaticText(this, ID_STATICTEXT4, _("Adress"), wxPoint(96,256), wxDefaultSize, 0, _T("ID_STATICTEXT4"));
    StaticText5 = new wxStaticText(this, ID_STATICTEXT5, _("Data"), wxPoint(96,296), wxDefaultSize, 0, _T("ID_STATICTEXT5"));
    TextCtrl7 = new wxTextCtrl(this, ID_TEXTCTRL7, _("Text"), wxPoint(168,88), wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL7"));
    MenuBar1 = new wxMenuBar();
    Menu1 = new wxMenu();
    MenuItem1 = new wxMenuItem(Menu1, idMenuQuit, _("Quit\tAlt-F4"), _("Quit the application"), wxITEM_NORMAL);
    Menu1->Append(MenuItem1);
    MenuBar1->Append(Menu1, _("&File"));
    Menu2 = new wxMenu();
    MenuItem2 = new wxMenuItem(Menu2, idMenuAbout, _("About\tF1"), _("Show info about this application"), wxITEM_NORMAL);
    Menu2->Append(MenuItem2);
    MenuBar1->Append(Menu2, _("Help"));
    SetMenuBar(MenuBar1);
    StatusBar1 = new wxStatusBar(this, ID_STATUSBAR1, 0, _T("ID_STATUSBAR1"));
    int __wxStatusBarWidths_1[1] = { -1 };
    int __wxStatusBarStyles_1[1] = { wxSB_NORMAL };
    StatusBar1->SetFieldsCount(1,__wxStatusBarWidths_1);
    StatusBar1->SetStatusStyles(1,__wxStatusBarStyles_1);
    SetStatusBar(StatusBar1);

    Connect(ID_TEXTCTRL3,wxEVT_COMMAND_TEXT_UPDATED,(wxObjectEventFunction)&klassGuiFrame::OnTextCtrl3Text);
    Connect(ID_BUTTON1,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&klassGuiFrame::OnButton1Click);
    Connect(idMenuQuit,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&klassGuiFrame::OnQuit);
    Connect(idMenuAbout,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&klassGuiFrame::OnAbout);
    //*)
}

klassGuiFrame::~klassGuiFrame()
{
    //(*Destroy(klassGuiFrame)
    //*)
}

void klassGuiFrame::OnQuit(wxCommandEvent& event)
{
    Close();
}

void klassGuiFrame::OnAbout(wxCommandEvent& event)
{
    wxString msg = wxbuildinfo(long_f);
    wxMessageBox(msg, _("Welcome to..."));
}

void klassGuiFrame::OnButton1Click(wxCommandEvent& event)
{

             Dane cz1;
             cz1.imie=TextCtrl1->GetValue();
             cz1.nazwisko=TextCtrl2->GetValue();
             cz1.telefon=TextCtrl3->GetValue();
             cz1.adress=TextCtrl4->GetValue();
             cz1.data=TextCtrl5->GetValue();
             TextCtrl6->SetValue(wxString(cz1.imie));
             fstream plik( "plik.txt");
             plik <<cz1.imie;
             plik.close();
             Layout();
}

void klassGuiFrame::OnTextCtrl3Text(wxCommandEvent& event)
{
}
