/***************************************************************
 * Name:      klassGuiApp.cpp
 * Purpose:   Code for Application Class
 * Author:     ()
 * Created:   2016-11-28
 * Copyright:  ()
 * License:
 **************************************************************/

#include "klassGuiApp.h"

//(*AppHeaders
#include "klassGuiMain.h"
#include <wx/image.h>
//*)

IMPLEMENT_APP(klassGuiApp);

bool klassGuiApp::OnInit()
{
    //(*AppInitialize
    bool wxsOK = true;
    wxInitAllImageHandlers();
    if ( wxsOK )
    {
    	klassGuiFrame* Frame = new klassGuiFrame(0);
    	Frame->Show();
    	SetTopWindow(Frame);
    }
    //*)
    return wxsOK;

}
