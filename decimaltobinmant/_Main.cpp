/***************************************************************
 * Name:      _Main.cpp
 * Purpose:   Code for Application Frame
 * Author:     ()
 * Created:   2016-11-21
 * Copyright:  ()
 * License:
 **************************************************************/

#include "_Main.h"
#include <wx/msgdlg.h>

#include <iostream>
#include <string>
#include <wx/string.h>
#include <wx/intl.h>


int converter(int a)
    {
      int b=0, k=1;
      while(a)
	  {

	  b+=a%2*k;
	  a/=2;
	  k*=10;

	}
	return b;
	}
	//helper functions
enum wxbuildinfoformat {
    short_f, long_f };

wxString wxbuildinfo(wxbuildinfoformat format)
{
    wxString wxbuild(wxVERSION_STRING);

    if (format == long_f )
    {
#if defined(__WXMSW__)
        wxbuild << _T("-Windows");
#elif defined(__UNIX__)
        wxbuild << _T("-Linux");
#endif

#if wxUSE_UNICODE
        wxbuild << _T("-Unicode build");
#else
        wxbuild << _T("-ANSI build");
#endif // wxUSE_UNICODE
    }

    return wxbuild;
}

//(*IdInit(_Frame)
const long _Frame::ID_BUTTON1 = wxNewId();
const long _Frame::ID_STATICTEXT1 = wxNewId();
const long _Frame::ID_TEXTCTRL1 = wxNewId();
const long _Frame::ID_TEXTCTRL2 = wxNewId();
const long _Frame::ID_TEXTCTRL3 = wxNewId();
const long _Frame::ID_TEXTCTRL4 = wxNewId();
const long _Frame::ID_MENUITEM1 = wxNewId();
const long _Frame::idMenuAbout = wxNewId();
const long _Frame::ID_STATUSBAR1 = wxNewId();
//*)

BEGIN_EVENT_TABLE(_Frame,wxFrame)
    //(*EventTable(_Frame)
    //*)
END_EVENT_TABLE()

_Frame::_Frame(wxWindow* parent,wxWindowID id)
{
    //(*Initialize(_Frame)
    wxMenuItem* MenuItem2;
    wxMenuItem* MenuItem1;
    wxMenu* Menu1;
    wxMenuBar* MenuBar1;
    wxMenu* Menu2;

    Create(parent, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxDEFAULT_FRAME_STYLE, _T("wxID_ANY"));
    SetClientSize(wxSize(352,384));
    Button1 = new wxButton(this, ID_BUTTON1, _("Label"), wxPoint(152,224), wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON1"));
    StaticText1 = new wxStaticText(this, ID_STATICTEXT1, _("Label"), wxPoint(136,184), wxDefaultSize, 0, _T("ID_STATICTEXT1"));
    Text1 = new wxTextCtrl(this, ID_TEXTCTRL1, wxEmptyString, wxPoint(32,272), wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL1"));
    Text2 = new wxTextCtrl(this, ID_TEXTCTRL2, wxEmptyString, wxPoint(32,224), wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL2"));
    Text3 = new wxTextCtrl(this, ID_TEXTCTRL3, wxEmptyString, wxPoint(136,272), wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL3"));
    Text4 = new wxTextCtrl(this, ID_TEXTCTRL4, wxEmptyString, wxPoint(240,272), wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL4"));
    MenuBar1 = new wxMenuBar();
    Menu1 = new wxMenu();
    MenuItem1 = new wxMenuItem(Menu1, ID_MENUITEM1, _("Quit\tAlt-F4"), _("Quit the application"), wxITEM_NORMAL);
    Menu1->Append(MenuItem1);
    MenuBar1->Append(Menu1, _("&File"));
    Menu2 = new wxMenu();
    MenuItem2 = new wxMenuItem(Menu2, idMenuAbout, _("About\tF1"), _("Show info about this application"), wxITEM_NORMAL);
    Menu2->Append(MenuItem2);
    MenuBar1->Append(Menu2, _("Help"));
    SetMenuBar(MenuBar1);
    StatusBar1 = new wxStatusBar(this, ID_STATUSBAR1, 0, _T("ID_STATUSBAR1"));
    int __wxStatusBarWidths_1[1] = { -1 };
    int __wxStatusBarStyles_1[1] = { wxSB_NORMAL };
    StatusBar1->SetFieldsCount(1,__wxStatusBarWidths_1);
    StatusBar1->SetStatusStyles(1,__wxStatusBarStyles_1);
    SetStatusBar(StatusBar1);

    Connect(ID_BUTTON1,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&_Frame::OnButton1Click);
    Connect(ID_TEXTCTRL1,wxEVT_COMMAND_TEXT_UPDATED,(wxObjectEventFunction)&_Frame::OnTextCtrl1Text);
    Connect(ID_TEXTCTRL2,wxEVT_COMMAND_TEXT_UPDATED,(wxObjectEventFunction)&_Frame::OnTextCtrl1Text1);
    Connect(ID_TEXTCTRL3,wxEVT_COMMAND_TEXT_UPDATED,(wxObjectEventFunction)&_Frame::OnTextCtrl1Text2);
    Connect(ID_MENUITEM1,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&_Frame::OnQuit);
    Connect(idMenuAbout,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&_Frame::OnAbout);
    //*)
}

_Frame::~_Frame()
{
    //(*Destroy(_Frame)
    //*)
}

void _Frame::OnQuit(wxCommandEvent& event)
{
    Close();
}

void _Frame::OnAbout(wxCommandEvent& event)
{
    wxString msg = wxbuildinfo(long_f);
    wxMessageBox(msg, _("Welcome to..."));
}



void _Frame::OnButton1Click(wxCommandEvent& event)
{
    float num;
    float num1;
    wxString Text = Text2->GetValue();
    num = wxAtoi(Text);
    num1=converter(num);


    wxString mystring = wxString::Format(wxT("%i"),num1);
    Text1->SetValue(wxString(mystring));
    Layout();
}

void _Frame::OnTextCtrl1Text(wxCommandEvent& event)
{

}

void _Frame::OnTextCtrl1Text1(wxCommandEvent& event)
{

}

void _Frame::OnTextCtrl1Text2(wxCommandEvent& event)
{
}
